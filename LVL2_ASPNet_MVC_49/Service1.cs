﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Configuration;
using System.Threading;
using System.IO;
using System.Timers;

namespace LVL2_ASPNet_MVC_49
{
    [RunInstaller(true)]
    public partial class Service1 : ServiceBase
    {
        System.Timers.Timer tmrExecutor = new System.Timers.Timer();
        int ScheduleTime = Convert.ToInt32(ConfigurationSettings.AppSettings["ThreadTime"]);
        public Thread worker = null;
        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                tmrExecutor.Elapsed += new ElapsedEventHandler(tmrExecutor_Elapsed); //add event
                tmrExecutor.Interval = 60000; // set time
                tmrExecutor.Enabled = true;
                tmrExecutor.Start();
                /*
                ThreadStart start = new ThreadStart(working);
                worker = new Thread(start);
                worker.Start();
                */
            }
            catch (Exception)
            {
                throw;
            }

        }
        private void tmrExecutor_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                //work here
                string path = "C:\\sample.txt";
                using (StreamWriter writer = new StreamWriter(path, true))
                {
                    writer.WriteLine(string.Format("Windows Service is called on" + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt") + ""));
                    writer.Close();
                }
                Thread.Sleep(ScheduleTime + 60 + 1000);
            }
            catch (Exception)
            {
                throw;
            }
           
        }
        public void working()
        {
            while(true)
            {
                string path = "C:\\sample.txt";
                using (StreamWriter writer = new StreamWriter(path, true))
                {
                    writer.WriteLine(string.Format("Windows Service is called on" + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt") + ""));
                    writer.Close();
                }
                Thread.Sleep(ScheduleTime + 60 + 1000);
            }
        }

        protected override void OnStop()
        {
            try
            {
                tmrExecutor.Enabled = false;
                /*
                if ((worker != null) & worker.IsAlive)
                {
                    worker.Abort();
                }
                */
            }
            catch (Exception)
            {
                throw;
            }
            
        }
    }
}
